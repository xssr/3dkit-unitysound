﻿using System.Collections;
using UnityEngine;
using NaughtyAttributes;

public class AudioCountdownDisableObject : MonoBehaviour
{
    [SerializeField]
    [InfoBox("This script will DISABLE the GameObject assigned to the VARIABLE below after countdown", EInfoBoxType.Normal)]

    [Tooltip("The time in seconds the script will COUNT DOWN from")]
    public float countDownFrom = 3.0f;
    [Tooltip("Assign the GameObject to be DISABLED here")]
    public GameObject gameObjectToDisable;
    private bool atzero = false;
    private float usersetvar;

    void Awake()
    {
        atzero = false;
        usersetvar = countDownFrom;
    }

    void Start()
    {
        if (gameObjectToDisable == null)
            Debug.Log("AUDIO WARNING - You have not assigned a GameObject on this AudioCountdownDisableObject script!", this);
    }


    private void OnEnable()
    {
        atzero = false;
        countDownFrom = usersetvar;
    }

    void Update()
    {
        if (atzero == false)
        {
            countDownFrom -= Time.deltaTime;
            if (countDownFrom < 0)
            {
                countDownFrom = 0.0f;
                gameObjectToDisable.SetActive(false);
                atzero = true;
                return;
            }
        }

       else if (atzero == true)
        {
            return;
        }

    }
}