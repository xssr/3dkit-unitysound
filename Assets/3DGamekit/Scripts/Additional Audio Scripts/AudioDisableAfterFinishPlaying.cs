﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using NaughtyAttributes;

public class AudioDisableAfterFinishPlaying : MonoBehaviour
    
{
    [SerializeField]
    [InfoBox("This script will DISABLE the assigned AudioSource GameObject below when the audio clip has FINISHED PLAYING!", EInfoBoxType.Normal)]

    [Tooltip("Assign the GameObject to be DISABLED AFTER IT HAS FINISHED PLAYING here")]
    public AudioSource DisableAudiosourceAfterPlay;

    void Start()
    {
        if (DisableAudiosourceAfterPlay == null)
            Debug.Log("AUDIO WARNING - You have not assigned an AudioSource to this AudioDisableAfterFinishPlaying script!", this);
    }

    void Update()
    {
    if (!DisableAudiosourceAfterPlay.isPlaying)
            DisableAudiosourceAfterPlay.gameObject.SetActive(false);

    }
}
