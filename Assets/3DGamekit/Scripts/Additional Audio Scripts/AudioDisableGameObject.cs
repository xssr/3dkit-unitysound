﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;

public class AudioDisableGameObject : MonoBehaviour
{
    [SerializeField]
    [InfoBox("This script will DISABLE the GameObject assigned to the variable below IMMEDIATELY", EInfoBoxType.Normal)]

    [Tooltip("Assign the GameObject to be IMMEDIATLY DISABLED here")]
    public GameObject GameObjectToDisable;

    void Start()
    {
        if (GameObjectToDisable != null) 
            GameObjectToDisable.SetActive(false);

        if (GameObjectToDisable == null)
            Debug.Log("AUDIO WARNING - You have not assigned an AudioSource on this AudioDisableGameObject script!", this);
    }


}


