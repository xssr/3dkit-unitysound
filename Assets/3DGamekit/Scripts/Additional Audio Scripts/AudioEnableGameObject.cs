﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;


public class AudioEnableGameObject : MonoBehaviour
{
    [SerializeField]
    [InfoBox("This script will ENABLE the GameObject assigned to the variable below IMMEDIATELY", EInfoBoxType.Normal)]

    [Tooltip("Assign the GameObject to be IMMEDIATLY ENABLED here")]
    public GameObject GameObjectToEnable;

    void Start()
    {
        if (GameObjectToEnable != null)
            GameObjectToEnable.SetActive(true);

        if (GameObjectToEnable == null)
            Debug.Log("AUDIO WARNING - You have not assigned an AudioSource on this AudioEnableGameObject script!", this);
    }

}
