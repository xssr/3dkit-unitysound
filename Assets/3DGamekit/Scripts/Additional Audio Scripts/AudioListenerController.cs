﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;

public class AudioListenerController : MonoBehaviour
{
    [InfoBox("This script will allow you to set the Audio Listener to Ellen's perspective", EInfoBoxType.Normal)]

    [Tooltip("Assign the ELLEN Parent GameObject here")]
    public GameObject Character;
    [Tooltip("Assign the CameraBrain GameObject here")]
    public Camera TheCamera;

    void Start()
    {
        if (Character == null)
            Debug.Log("AUDIO WARNING - You have not assigned the Ellen GameObject to the Character variable on this AudioListenerController script!", this);

        if (TheCamera == null)
            Debug.Log("AUDIO WARNING - You have not assigned The CameraBrain GameObject to the The Camera variable on this AudioListenerController script!", this);
    }

    void Update()
    {

        transform.position = Character.transform.position;
        transform.rotation = TheCamera.transform.rotation;
        
    }
}
