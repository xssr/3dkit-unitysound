﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;

namespace Gamekit3D
{ 
public class AudioPlayRandomAudioPlayerScript : MonoBehaviour
    {
        [InfoBox("This script will play the RandomAudioPlayer GameObject attached to the varable below when this GameObject is Enabled", EInfoBoxType.Normal)]

        [Tooltip("Assign the RandomAudioPlayer GameObject to be played here")]
        public RandomAudioPlayer RandomAudioPlayerGameObject;

        void Start()
        {
            if (RandomAudioPlayerGameObject == null)
                Debug.Log("AUDIO WARNING - You have not assigned a GameObject containing a RandomAudioPlayer to this AudioPlayRandomAudioPlayer script!", this);
        }

        void OnEnable()
        {
            if (RandomAudioPlayerGameObject != null)
                RandomAudioPlayerGameObject = RandomAudioPlayerGameObject.GetComponent<RandomAudioPlayer>();

                RandomAudioPlayerGameObject.PlayRandomClip();
        }

        void Reset()
        {
            if (RandomAudioPlayerGameObject != null)
                RandomAudioPlayerGameObject = RandomAudioPlayerGameObject.GetComponent<RandomAudioPlayer>();
        }

    }
}
