﻿using UnityEngine;
using UnityEngine.Audio;
using NaughtyAttributes;

public class AudioSourceFadeInVolume: MonoBehaviour

{
    [SerializeField]
    [InfoBox("This script will fade IN the assigned AudioSource from it's current volume to the volume set on the TARGET AUDIO VOLUME variable below over the amount of time set on the FADE IN TIME variable", EInfoBoxType.Normal)]

    [Tooltip("Set the FADE IN time in SECONDS")]
    private float m_FadeInTimeSeconds = 1.0f;
    [Tooltip("Assign the AudioSource to FADE UP the volume of here")]
    public AudioSource m_AudioSource;
    [Tooltip("Set the volume to FADE UP to")]
    [Range(0, 1)] public float TargetAudioVolume = 1.0f;


    private void Start()
    {
        m_AudioSource.volume = 0.0f;

        if (m_AudioSource == null)
            Debug.Log("AUDIO WARNING - You have not assigned a GameObject containing an AudioSource to this AudioSourceFadeInVolume script!", this);
    }

    void OnEnable()
    {
        m_AudioSource.volume = 0.0f;
    }

    private void Update()
    {
        if (m_AudioSource.volume < TargetAudioVolume)
        {
            m_AudioSource.volume = m_AudioSource.volume + (Time.deltaTime / (m_FadeInTimeSeconds + 1));

        }

        if (m_AudioSource.volume >= TargetAudioVolume)
        {
            m_AudioSource.volume = TargetAudioVolume;
        }

    }
}