﻿using UnityEngine;
using UnityEngine.Audio;
using NaughtyAttributes;

public class AudioSourceFadeOutVolume : MonoBehaviour

{
    [SerializeField]
    [InfoBox("This script will fade out the assigned AudioSource from it's current volume to the volume set on the TARGET AUDIO VOLUME variable below over the amount of time set on the FADE OUT TIME variable", EInfoBoxType.Normal)]

    [Tooltip("Set the FADE OUT time in SECONDS")]
    private float m_FadeOutTimeSeconds = 1.0f;
    [Tooltip("Assign the AudioSource to FADE OUT the volume of here")]
    public AudioSource m_AudioSource;
    [Tooltip("Set the volume to FADE DOWN to")]
    [Range(0, 1)] public float TargetAudioVolume = 0.0f;

    private void Start()
    {
        if (m_AudioSource == null)
            Debug.Log("AUDIO WARNING - You have not assigned a GameObject containing an AudioSource to this AudioSourceFadeOutVolume script!", this);
    }


    private void Update()
    {
        if (m_AudioSource.volume > TargetAudioVolume)
        {
            m_AudioSource.volume = m_AudioSource.volume - (Time.deltaTime / (m_FadeOutTimeSeconds + 1));

        }

        if (m_AudioSource.volume <= TargetAudioVolume)
        {
            m_AudioSource.volume = TargetAudioVolume;
        }

        if (m_AudioSource.volume == TargetAudioVolume)

        {
         //   if (DisableThisGameObjectAtTargetVolume == true)
         //       gameObject.SetActive(false);
        }

        else
        {
        }
    }
    
}