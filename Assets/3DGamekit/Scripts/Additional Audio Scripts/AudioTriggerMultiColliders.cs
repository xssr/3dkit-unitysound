﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEditor;
using NaughtyAttributes;


namespace Gamekit3D
{
    [RequireComponent(typeof(Collider))]
    public class AudioTriggerMultiColliders : MonoBehaviour
    {
        [InfoBox("This button will automatically create a NEW Child GameObject and will allow you to make more complex shapes with overlapping Collider Areas.", EInfoBoxType.Normal)]
        [InfoBox("This script has the same functionality as the INTERACT ON TRIGGER script with the addition of the blue ADD CHILD COLLIDERS button.", EInfoBoxType.Normal)]

        public LayerMask layers;
        public UnityEvent OnEnter, OnExit;
        new Collider collider;
        public InventoryController.InventoryChecker[] inventoryChecks;

        private bool m_disableGameObjectsOnAllCollidersExit = true;

        int m_referenceCount = 0;

        void Reset()
        {
            gameObject.layer = LayerMask.NameToLayer("Collider");
            layers = LayerMask.NameToLayer("Everything");
            collider = GetComponent<Collider>();
            collider.isTrigger = true;
        }

        void OnTriggerEnter(Collider other)
        {
            if (0 != (layers.value & 1 << other.gameObject.layer))
            {
                ++m_referenceCount;
                ExecuteOnEnter(other);
            }
        }

        protected virtual void ExecuteOnEnter(Collider other)
        {
            OnEnter.Invoke();
            for (var i = 0; i < inventoryChecks.Length; i++)
            {
                inventoryChecks[i].CheckInventory(other.GetComponentInChildren<InventoryController>());
            }
        }

        void OnTriggerExit(Collider other)
        {
            --m_referenceCount;
            if (m_disableGameObjectsOnAllCollidersExit && m_referenceCount == 0)
            {

                if (0 != (layers.value & 1 << other.gameObject.layer))
                {
                    ExecuteOnExit(other);
                }
            }
        }

        public void OnChildTriggerEnter(Collider other)
        {
            OnTriggerEnter(other);
        }

        public void OnChildTriggerExit(Collider other)
        {
            OnTriggerExit(other);
        }

        protected virtual void ExecuteOnExit(Collider other)
        {
            OnExit.Invoke();
        }

        void OnDrawGizmos()
        {
            Gizmos.DrawIcon(transform.position, "InteractionTrigger", false);
        }

        void OnDrawGizmosSelected()
        {
        }

    } 
}

#if UNITY_EDITOR
[UnityEditor.CustomEditor(typeof(Gamekit3D.AudioTriggerMultiColliders))]
public class AudioTriggerAdvancedEditorFace : UnityEditor.Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();
        Color col = GUI.color;
        GUI.color = Color.cyan;
        if (GUILayout.Button("Add Child Collider"))
        {
            Gamekit3D.AudioTriggerMultiColliders parent = target as Gamekit3D.AudioTriggerMultiColliders;

            GameObject child = new GameObject("Additional Child Collider");
            Undo.RegisterCreatedObjectUndo(child, "Created Child Trigger");
            child.transform.SetParent(parent.transform);
            child.transform.localPosition = Vector3.zero;
            child.transform.localRotation = Quaternion.identity;
            child.layer = LayerMask.NameToLayer("Collider");
            BoxCollider collider = child.AddComponent<BoxCollider>();
            collider.isTrigger = true;
            child.AddComponent<AudioChildColliderTrigger>();
            Selection.activeGameObject = child.gameObject;
        }
        GUI.color = col;
    }
}
#endif
