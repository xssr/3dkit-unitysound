﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;

namespace Gamekit3D
{
    public class DamageableEnableGameObject : MonoBehaviour
    {
        [InfoBox("This script will ENABLE a GameObject when the assigned character's REMAINING HEALTH reaches the value specified. It will then DISABLE the same GameObject if the character's Health is restored", EInfoBoxType.Normal)]
        [Tooltip("Assign a GameObject containing a Damageable script component")]
        public Damageable AssignGameObjectContainingDamagableComponent;
        [Tooltip("Enable GameObject below when character's REMAINING HEALTH reaches this value")]
        public int EnableWhenHealthRemainingAt;
        [Tooltip("Assign a GameObject to be ENABLED when character's REMAINING HEALTH is at the value above")]
        public GameObject GameObjectToEnable;

        void Start()
        {
            if (AssignGameObjectContainingDamagableComponent == null)
                Debug.Log("AUDIO WARNING - You have not assigned a GameObject containing a Damageable Component to the AssignGameObjectContainingDamagableComponent variable on this DamageableEnableGameObject script!", this);

            if (GameObjectToEnable == null)
                Debug.Log("AUDIO WARNING - You have not assigned a GameObject to the GameObjectToEnable variable on this DamageableEnableGameObject script!", this);
        }

        void Update()
        {

            if (AssignGameObjectContainingDamagableComponent.currentHitPoints > EnableWhenHealthRemainingAt)
            {
                GameObjectToEnable.SetActive(false);
            }

            if (AssignGameObjectContainingDamagableComponent.currentHitPoints <= EnableWhenHealthRemainingAt)
            {
                GameObjectToEnable.SetActive(true);
            }

        }
    }
}
