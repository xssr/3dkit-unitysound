﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;

public class MuteAudioSource : MonoBehaviour

{
    [InfoBox("This script will MUTE the assigned AudioSource IMMEDIATELY", EInfoBoxType.Normal)]

    [Tooltip("Assign the AudioSource GameObject to be MUTED")]
    public AudioSource m_MuteAudioSource;

    void Start()
    {
        if (m_MuteAudioSource != null)
            m_MuteAudioSource.mute = m_MuteAudioSource;

            if (m_MuteAudioSource == null)
                Debug.Log("AUDIO WARNING - You have not assigned a GameObject containing an AudioSource to this MuteAudioSource script!", this);
    }
}