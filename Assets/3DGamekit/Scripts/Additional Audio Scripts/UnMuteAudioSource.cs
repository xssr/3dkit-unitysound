﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;

public class UnMuteAudioSource : MonoBehaviour

{
    [InfoBox("This script will UNMUTE the assigned AudioSource IMMEDIATELY", EInfoBoxType.Normal)]

    [Tooltip("Assign the AudioSource GameObject to be UNMUTED")]
    public AudioSource m_UnMuteAudioSource;

    void Start()
    {
        if (m_UnMuteAudioSource != null)
            m_UnMuteAudioSource.mute = !m_UnMuteAudioSource;

        if (m_UnMuteAudioSource == null)
            Debug.Log("AUDIO WARNING - You have not assigned a GameObject containing an AudioSource to this MuteAudioSource script!", this);
    }
}