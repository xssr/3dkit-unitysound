﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;



[RequireComponent(typeof(Collider))]
public class AudioChildColliderTrigger : MonoBehaviour
{
    [InfoBox("This script and GameObject are Automatically created when clicking on the ADD CHILD COLLIDER button on Parent GameObject", EInfoBoxType.Normal)]

    Gamekit3D.AudioTriggerMultiColliders m_parent;

    void OnTriggerEnter(Collider other)
    {
        if (m_parent == null)
            m_parent = GetComponentInParent<Gamekit3D.AudioTriggerMultiColliders>();

        if (m_parent)
            m_parent.OnChildTriggerEnter(other);
    }
    ////////////////////////////////////////

    void OnTriggerExit(Collider other)
    {
        if (m_parent == null)
            m_parent = GetComponentInParent<Gamekit3D.AudioTriggerMultiColliders>();

        if (m_parent)
            m_parent.OnChildTriggerExit(other);
    }
    ////////////////////////////////////////
}
