﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;
using NaughtyAttributes;


namespace Gamekit3D
{
    [RequireComponent(typeof(AudioSource))]
    public class RandomAudioPlayer : MonoBehaviour
    {
        [Serializable]
        public class MaterialAudioOverride
        {
            public Material[] materials;
            public SoundBank[] banks;
        }

        [Serializable]
        public class SoundBank
        {
            public string name;
            public AudioClip[] AudioClips;
        }
        [InfoBox("This script will allow you to select a RANDOM amount of PITCH and VOLUME variation as well as additional AudioClip variations that will be RANDOMLY selected each time the sound is played", EInfoBoxType.Normal)]

        [Tooltip("The MINIMUM Random Volume to playback")]
        [Range(0, 1)] public float minVolume = 1.0f;
        [Tooltip("The MAXIMUM Random Volume to playback")]
        [Range(0, 1)] public float maxVolume = 1.0f;
        [Tooltip("The MINIMUM Random Pitch to playback")]
        [Range(-3, 3)] public float minPitch = 1.0f;
        [Tooltip("The MAXIMUM Random Pitch to playback.")]
        [Range(-3, 3)] public float maxPitch = 1.0f;
        [Tooltip("Delay Sound playback in Seconds.")]
        public float playDelay = 0;
        [Tooltip("Add your Audioclips to this DEFAULT Bank")]
        public SoundBank defaultBank = new SoundBank();
        [InfoBox("The Overrides below are only used for Ellen's Footsteps and Rolling SFX", EInfoBoxType.Warning)]
        [Tooltip("Only used for Ellen's Footsteps & Rolling SFX!!!")]
        public MaterialAudioOverride[] overrides;

        [HideInInspector]
        public bool playing;
        [HideInInspector]
        public bool canPlay;

        protected AudioSource m_Audiosource;
        protected Dictionary<Material, SoundBank[]> m_Lookup = new Dictionary<Material, SoundBank[]>();

        public AudioSource audioSource { get { return m_Audiosource; } }

        public AudioClip clip { get; private set; }

        void Awake()
        {
            m_Audiosource = GetComponent<AudioSource>();
            for (int i = 0; i < overrides.Length; i++)
            {
                foreach (var material in overrides[i].materials)
                    m_Lookup[material] = overrides[i].banks;
            }
        }

        public AudioClip PlayRandomClip(Material overrideMaterial, int bankId = 0)
        {
#if UNITY_EDITOR
        
#endif
            if (overrideMaterial == null) return null;
            return InternalPlayRandomClip(overrideMaterial, bankId);
        }

        
        public void PlayRandomClip()
        {
            clip = InternalPlayRandomClip(null, bankId: 0);
        
            if (clip == null)
               Debug.Log("AUDIO WARNING - You have not assigned any AudioClip/s to the DEFAULT BANK of this RandomAudioPlayer Script!", this);
        }

        AudioClip InternalPlayRandomClip(Material overrideMaterial, int bankId)
        {
            SoundBank[] banks = null;
            var bank = defaultBank;
            if (overrideMaterial != null)
                if (m_Lookup.TryGetValue(overrideMaterial, out banks))
                    if (bankId < banks.Length)
                        bank = banks[bankId];
            if (bank.AudioClips == null || bank.AudioClips.Length == 0)
                return null;
            var clip = bank.AudioClips[Random.Range(0, bank.AudioClips.Length)];

            if (clip == null)
                return null;

            audioSource.volume = Random.Range(minVolume, maxVolume);
            audioSource.pitch = Random.Range(minPitch, maxPitch);
            m_Audiosource.clip = clip;
            m_Audiosource.PlayDelayed(playDelay);

            return clip;
        }

    }
}
