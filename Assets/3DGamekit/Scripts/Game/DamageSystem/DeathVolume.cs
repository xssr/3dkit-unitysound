﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Gamekit3D
{
    [RequireComponent(typeof(Collider))]
    public class DeathVolume : MonoBehaviour
    {
        public LayerMask layers;
        public RandomAudioPlayer RandomSplashSFX;

        void OnTriggerEnter(Collider other)
        {
            var pc = other.GetComponent<PlayerController>();
            if (pc != null)
            {
                pc.Die(new Damageable.DamageMessage());
            }


            if(0 != (layers.value & 1 << other.gameObject.layer)){

                if (RandomSplashSFX != null)
                {
                    if (!RandomSplashSFX.audioSource.isPlaying)
                    {
                        RandomSplashSFX.transform.position = other.transform.position;
                        RandomSplashSFX.PlayRandomClip();
                    }

                    else { }
                }

            }
           
        }

        void Reset()
        {
            if (LayerMask.LayerToName(gameObject.layer) == "Default")
                gameObject.layer = LayerMask.NameToLayer("Environment");
            var c = GetComponent<Collider>();
            if (c != null)
                c.isTrigger = true;

            if (RandomSplashSFX != null)
                RandomSplashSFX = RandomSplashSFX.GetComponent<RandomAudioPlayer>();
        }

    }
}
