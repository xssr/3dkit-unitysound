﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using NaughtyAttributes;

public class DestroyAfterFinishPlaying : MonoBehaviour
    
{
    [SerializeField]
    [InfoBox("This script will Destroy/remove this GameObject from the Hierarchy when the audio clip has finished playing", EInfoBoxType.Normal)]


    public AudioSource DestroyAudiosourceAfterPlay;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
    if (!DestroyAudiosourceAfterPlay.isPlaying)
            Destroy(gameObject);

    }
}
