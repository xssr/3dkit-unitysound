﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;

namespace Gamekit3D
{
    public class InteractiveMusicController : MonoBehaviour
    {
        [SerializeField]
        [InfoBox("Assign the primary music AudioSource GameObject to the topmost variable. This will be heard when outside of the Secondary Colliders below", EInfoBoxType.Normal)]

        public float AudioSourceMaxVolume = 1;
        public float StartVolume = 0;
        public float volumeFadeTime = 4;
        public bool playOnStart = true;

        public AudioSource[] audioSources;
        AudioSource activeAudio, fadeAudio;
        float volumeVelocity, fadeVelocity;
        float volume;
        Stack<string> trackStack = new Stack<string>();

        public void PushTrack(string name)
        {
            trackStack.Push(name);
            Enqueue(name);
        }

        public void PopTrack()
        {
            if (trackStack.Count > 1)
                trackStack.Pop();
            Enqueue(trackStack.Peek());
        }

        public void Enqueue(string name)
        {
            foreach (var i in audioSources)
            {
                if (i.name == name)
                {
                    fadeAudio = activeAudio;
                    activeAudio = i;
                    if (!activeAudio.isPlaying) activeAudio.Play();
                    break;
                }
            }
        }

        public void Play()
        {
            if (activeAudio != null)
                activeAudio.Play();
        }

        public void Stop()
        {
            foreach (var i in audioSources) i.Stop();
        }

        void OnEnable()
        {
            trackStack.Clear();
            if (audioSources.Length > 0)
            {
                activeAudio = audioSources[0];
                foreach (var i in audioSources) i.volume = 0;
                trackStack.Push(audioSources[0].name);
                if (playOnStart) Play();
            }
            volume = StartVolume;
        }

        void Reset()
        {
            audioSources = GetComponentsInChildren<AudioSource>();
        }

        public void SetVolume(float volume)
        {
            this.volume = volume;
        }

        void Update()
        {
            if (activeAudio != null)
                activeAudio.volume = Mathf.SmoothDamp(activeAudio.volume, volume * AudioSourceMaxVolume, ref volumeVelocity, volumeFadeTime, 1);

            if (fadeAudio != null)
            {
                fadeAudio.volume = Mathf.SmoothDamp(fadeAudio.volume, 0, ref fadeVelocity, volumeFadeTime, 1);
                if (Mathf.Approximately(fadeAudio.volume, 0))
                {
                    fadeAudio.Stop();
                    fadeAudio = null;
                }
            }
        }
    }
}
