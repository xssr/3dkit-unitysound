﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Gamekit3D
{
    public class SecondaryInteractiveMusic : MonoBehaviour
    {
        public LayerMask layers;
        InteractiveMusicController secondaryinteractiveMusic;

        void OnEnable()
        {
            secondaryinteractiveMusic = GetComponentInParent<InteractiveMusicController>();
        }

        void OnTriggerEnter(Collider other)
        {
            if (0 != (layers.value & 1 << other.gameObject.layer))
                secondaryinteractiveMusic.PushTrack(this.name);
        }

        void OnTriggerExit(Collider other)
        {
            if (0 != (layers.value & 1 << other.gameObject.layer))
                secondaryinteractiveMusic.PopTrack();
        }

    }
}
