﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class AmbientFadein : MonoBehaviour
{
    public AudioMixer AudioMixer;
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(FadeMixerGroup.StartFade(AudioMixer, "AmbientVolume", 4,  1));
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
