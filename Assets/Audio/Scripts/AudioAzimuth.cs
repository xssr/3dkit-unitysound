﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioAzimuth : MonoBehaviour
{
   
    public AnimationCurve animationCurve;

    private AudioSource _source;

    private float azimuth;
    private float volume;
    private GameObject _camera;

    void Start()
    {
        _source = GetComponent<AudioSource>();
        volume = _source.volume;
        _camera = GameObject.FindWithTag("MainCamera");
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 direction = _camera.transform.position - transform.position;
        azimuth = (Vector3.Angle(_camera.transform.forward, direction)) / 180;
        _source.volume = volume * animationCurve.Evaluate(azimuth);
    }
}
