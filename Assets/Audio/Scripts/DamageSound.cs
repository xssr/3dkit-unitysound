﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]

public class DamageSound : MonoBehaviour
{
    public AudioClip[] hitsound;
    public AudioClip[] destructionsound;

    public AudioSource _sourcehit;
    public AudioSource _sourcedestruction;
    private int newIndex;

    private int last1Index;
    // Start is called before the first frame update

    


    public void OnHit()
    {
        Randomization(hitsound.Length);
        _sourcehit.clip = hitsound[newIndex];
        last1Index = newIndex;
        _sourcehit.Play();
    }
    void Randomization(int cliplenght)
    {
        if (cliplenght != 0)
        {
            newIndex = Random.Range(0, cliplenght);

            while (newIndex == last1Index )
                newIndex = Random.Range(0, cliplenght);
        }
    }

    public void OnDestruction()
    {
        Randomization(destructionsound.Length);
        _sourcedestruction.clip = destructionsound[newIndex];
        last1Index = newIndex;
        _sourcedestruction.Play();
       
    }
}
