﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

[RequireComponent(typeof(AudioSource))]
public class EelDogAnimationAudio : MonoBehaviour
{
    private AudioSource Source;
    public AudioClip[] jumpClip;
    public AudioClip[] landClip;
    public AnimationCurve animationCurve;
    
    GameObject _camera;
    float volume;
    private float azimuth;
    
    
    
    int lastIndex = 0; 
    int newIndex = 0; 
    
     // Start is called before the first frame update
   
    void Start()
    {
        Source = GetComponent<AudioSource>();
        volume = Source.volume;
        _camera = GameObject.FindWithTag("MainCamera");
    }

    private void Update()
    {
        Vector3 direction = _camera.transform.position - transform.position;
        azimuth = (Vector3.Angle(_camera.transform.forward, direction)) / 180;
        
       
    }

    void Jump()
    {
        PlaySound(jumpClip);
    }

    void Land()
    {
        PlaySound(landClip);
    }

    // Update is called once per frame
    void PlaySound(AudioClip[] RandomClips)
    {
        if (RandomClips.Length != 1 )
            Randomization(RandomClips.Length);
        
        Source.volume = Random.Range(0.8f, 1f) * animationCurve.Evaluate(azimuth);
        Source.pitch = Random.Range(1, 1.1f);
        
        Source.PlayOneShot(RandomClips[newIndex]);
        lastIndex = newIndex;
    }
    
    void Randomization(int cliplenght) 
    {
        
        newIndex = Random.Range(0, cliplenght);
        
        while (newIndex == lastIndex)
            newIndex = Random.Range(0, cliplenght); 
    }
}
