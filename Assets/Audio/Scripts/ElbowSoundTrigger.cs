﻿using System;
using System.Collections;
using System.Collections.Generic;
using Gamekit3D;
using Gamekit3D.GameCommands;
using UnityEngine;
using Random = UnityEngine.Random;

[RequireComponent(typeof(AudioSource))]
public class ElbowSoundTrigger : MonoBehaviour
{
    public Collider ElbowTrigger;
    public Animator _animator;
    public AudioClip[] ClothWalk;
    public AudioClip[] ClothRun;
    public AudioClip[] ClothLand;
    public AnimationCurve movementCurve;
    private AudioSource _source;
    private float volume;
    
    
    private int last1Index = 0;
    private int last2Index = 0;
    private int last3Index = 0;
    private int newIndex = 0;
    private void Start()
    {
        _source = gameObject.GetComponent<AudioSource>();
        volume = _source.volume;
    }
    
    

    private void OnTriggerEnter(Collider other)
        {
           
            if (other == ElbowTrigger)
            {
                PlayClothesSound();
            }
        }


    void PlayClothesSound()
    {
        
        AudioClip clip = null;
        float speed = _animator.GetFloat("ForwardSpeed");
        if (speed < 4)
        {
            Randomization(ClothWalk.Length);
            clip = ClothWalk[newIndex];
        }
        else if (speed >= 4)
        {
            Randomization(ClothRun.Length);
            clip = ClothRun[newIndex];
        }

        else
        {
            Randomization(ClothWalk.Length);
            clip = ClothWalk[newIndex];
        }
        
        float VolumeCoeff = speed / 7;
        _source.volume = movementCurve.Evaluate(VolumeCoeff) * Random.Range(0.9f, 1f);
        _source.pitch = Random.Range(0.95f, 1.05f);
        _source.PlayOneShot(clip);
        
        
        
        last1Index = last2Index;
        last2Index = last3Index;
        last3Index = newIndex;
    }
    
    void Randomization(int cliplenght) 
    {
        if (cliplenght != 0)
        {
            newIndex = Random.Range(0, cliplenght);
        
            while (newIndex == last1Index || newIndex == last2Index || newIndex == last3Index ) 
                newIndex = Random.Range(0, cliplenght); 
        }
        
        
    }
    
    
}
