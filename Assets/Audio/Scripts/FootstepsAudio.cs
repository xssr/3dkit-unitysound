﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using Gamekit3D;
using UnityEngine;
using Random = UnityEngine.Random;

public class FootstepsAudio : MonoBehaviour
{
    
    public GameObject LeftFoot;
    public GameObject RightFoot;
    private Animator animator;
    public bool walking;
    public bool running;
    public AnimationCurve movementCurve;
    public string Surface;
    
        
    [HideInInspector] public bool LeftPlaying;
    [HideInInspector] public bool LeftcanPlay;
    [HideInInspector] public bool RightPlaying;
    [HideInInspector]  public bool RightcanPlay;
    
    private PlayerController controller;

    public AudioClip[] WalkDirt;
    public AudioClip[] RunDirt;
    public AudioClip[] WalkGrass;
    public AudioClip[] RunGrass;
    public AudioClip[] WalkLeaves;
    public AudioClip[] RunLeaves;
    public AudioClip[] WalkStone;
    public AudioClip[] RunStone;
    [Range(0, 1)] public float LandVolume = 1f;
    public AudioClip[] LandDirt;
    public AudioClip[] LandMud;
    public AudioClip[] LandGrass;
    public AudioClip[] LandLeaves;
    public AudioClip[] LandStone;
    [Range(0, 1)] public float JumpVolume = 1f;
    public AudioClip[] JumpMud;
    public AudioClip[] JumpLeaves;
    public AudioClip[] JumpDirt;
    public AudioClip[] JumpGrass;
    public AudioClip LandRollClip;


    private int last1Index = 0;
    private int last2Index = 0;
    private int last3Index = 0;
    private int newIndex = 0;
    public LayerMask lm;

    public bool Leaves;
    public bool Grass;
   
    
    // Start is called before the first frame update
    void Start()
    {
        animator = GetComponent<Animator>();
        controller = GetComponent<PlayerController>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        PlaysoundfootLeft();
        PlaysoundfootRight();
     
    }
    
    
    
    
     void PlaysoundfootLeft()
    {
        
        float footfallCurve = animator.GetFloat("footLeft");
        float ForwardSpeed = animator.GetFloat("ForwardSpeed");
        if (footfallCurve > 0.01f && !LeftPlaying && LeftcanPlay)
        {
            Playfootstep(LeftFoot, ForwardSpeed);
            LeftPlaying = true;
            LeftcanPlay = false;
        }
        else if (LeftPlaying)
        {
            LeftPlaying = false;
        }
        
        else if (footfallCurve < 0.01f && !LeftcanPlay)
        {
            LeftcanPlay = true;
        }
    }
    
    void PlaysoundfootRight()
    {
         
        float footfallCurve = animator.GetFloat("footRight");
        float ForwardSpeed = animator.GetFloat("ForwardSpeed");
        if (footfallCurve > 0.01f && !RightPlaying && RightcanPlay)
        {
            Playfootstep(RightFoot, ForwardSpeed);
            RightPlaying = true;
            RightcanPlay = false;
        }
        else if (RightPlaying)
        {
            RightPlaying = false;
        }
        
        else if (footfallCurve < 0.01f && !RightcanPlay)
        {
            RightcanPlay = true;
        }

    }

    void Playfootstep(GameObject footObject, float speed)
    {
        AudioClip clip = null;
        SurfaceCheck(footObject);
        if (speed <= 4f)
        {
            if (Leaves)
            {
                if (WalkLeaves.Length != 1 ) Randomization(WalkLeaves.Length);
                clip = WalkLeaves[newIndex];
            }
            else if (Surface == "Grass"){
                if (WalkGrass.Length != 1 ) Randomization(WalkGrass.Length);
                clip = WalkGrass[newIndex];
            }
            
            else if (Surface == "Stone")
            {
                if (WalkStone.Length != 1 ) Randomization(WalkStone.Length);
                clip = WalkStone[newIndex];
            }
            else
            {
                if (WalkDirt.Length != 1 )
                    Randomization(WalkDirt.Length);
                clip = WalkDirt[newIndex];
            }
            
        }
        else if (speed > 4f)
        {
            walking = false;
            running = true;

            if (Leaves)
            {
                if (RunLeaves.Length != 1 ) Randomization(RunLeaves.Length);
                clip = RunLeaves[newIndex];
            }
            else if (Surface == "Stone")
            {
                if (RunStone.Length != 1 ) Randomization(RunStone.Length);
                clip = RunStone[newIndex];
            }
            else if(Surface == "Grass")
            {
                if (RunGrass.Length != 1 ) Randomization(RunGrass.Length);
                clip = RunGrass[newIndex];
            }
            else
            {
                if (RunDirt.Length != 1 )
                    Randomization(RunDirt.Length);
                clip = RunDirt[newIndex];
            }
            
            
        }
        
        AudioSource Source = footObject.GetComponent<AudioSource>();
        float VolumeCoeff = speed / 7;
        Source.volume = movementCurve.Evaluate(VolumeCoeff) * Random.Range(0.9f, 1f);
        Source.pitch = Random.Range(0.95f, 1.05f);
        Source.PlayOneShot(clip);
        
        
        
        last1Index = last2Index;
        last2Index = last3Index;
        last3Index = newIndex;
        
    }
    
    void Randomization(int cliplenght) 
    {
        if (cliplenght != 0)
        {
            newIndex = Random.Range(0, cliplenght);
        
            while (newIndex == last1Index || newIndex == last2Index || newIndex == last3Index ) 
                newIndex = Random.Range(0, cliplenght); 
        }
        
        
    }

    void SurfaceCheck(GameObject foot)
    {
        
        
        if (Physics.Raycast(foot.transform.position, Vector3.down, out RaycastHit hit, 0.3f, lm))
        {
            Surface = hit.collider.tag;
        }
        else
        {
            Surface = null;
        }
        
        
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.tag == "Leaves")
        {
            Leaves = true;
        }
       
    }

    private void OnTriggerExit(Collider other)
    {
        Leaves = false;
    }

    void OnLanding()
    {
        AudioClip clip = null;
        SurfaceCheck(LeftFoot);
        
            if (Leaves)
            {
                if (LandLeaves.Length != 1 ) Randomization(LandLeaves.Length);
                clip = LandLeaves[newIndex];
            }
            else if (Surface == "Stone")
            {
                if (LandStone.Length != 1 ) Randomization(LandStone.Length);
                clip = LandStone[newIndex];
            }
            else if(Surface == "Grass")
            {
                if (LandGrass.Length != 1 ) Randomization(LandGrass.Length);
                clip = LandGrass[newIndex];
            }
            else
            {
                if (LandDirt.Length != 1 )
                    Randomization(LandDirt.Length);
                clip = LandDirt[newIndex];
            }
            
            
        
        
        AudioSource Source = LeftFoot.GetComponent<AudioSource>();
        Source.volume = LandVolume * Random.Range(0.9f, 1f);
        Source.pitch = Random.Range(0.95f, 1.05f);
        Source.PlayOneShot(clip);
        
        
        
        last1Index = last2Index;
        last2Index = last3Index;
        last3Index = newIndex;
        

    }

    void JumpSound()
    {
        AudioClip clip = null;
        
        if (Leaves)
        {
            if (JumpLeaves.Length != 1) Randomization(JumpLeaves.Length);
            clip = JumpLeaves[newIndex];
        }

        else if (Surface == "Grass")
        {
            if (JumpGrass.Length != 1) Randomization(JumpGrass.Length);
            clip = JumpGrass[newIndex];
        }
        
        else
        {
            if (JumpDirt.Length != 1)
                Randomization(JumpDirt.Length);
            clip = JumpDirt[newIndex];
        }

    


        AudioSource Source = LeftFoot.GetComponent<AudioSource>();
        Source.volume = JumpVolume * Random.Range(0.9f, 1f);
        Source.pitch = Random.Range(0.95f, 1.05f);
        Source.PlayOneShot(clip);



        last1Index = last2Index;
        last2Index = last3Index;
        last3Index = newIndex;


    }


    void LandRoll()
    { 
       
        AudioSource Source = LeftFoot.GetComponent<AudioSource>();
        Source.volume = JumpVolume * Random.Range(0.9f, 1f);
        Source.pitch = Random.Range(0.95f, 1.05f);
        Source.PlayOneShot(LandRollClip);
        
    }
}
