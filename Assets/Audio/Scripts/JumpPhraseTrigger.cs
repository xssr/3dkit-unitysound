﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class JumpPhraseTrigger : MonoBehaviour
{
    public Collider jumptrigger;

    public AudioClip[] JumpPhraseClip;
    public AudioSource _source;
    private bool isPlayed;

    private void OnTriggerEnter(Collider other)
    {
        if (!isPlayed)
        {
            if (other == jumptrigger)
            {
           
                int clip = Random.Range(0, JumpPhraseClip.Length);
                _source.clip = JumpPhraseClip[clip];
                _source.Play();
                isPlayed = true;
            }
        }
        
    }
}
