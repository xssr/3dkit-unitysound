﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class MusicDrone : MonoBehaviour
{
    public AnimationCurve _curve;
    private AudioSource _source;
    public float fadetime;
    private float fade;
    private float volume;
    public bool RandomPlaybackPosition;
    public AudioClip _clip;
    public AnimationCurve LFO;

    private float currentTime = 0f;
    public float LFOTime;
    float LFOCurrentTime;
    private float currentVolume;
    public AudioClip drone2;
    public AudioMixerGroup group;
    
    
    // Start is called before the first frame update
    void Start()
    {
        _source = GetComponent<AudioSource>();
        volume = _source.volume;
        _source.clip = _clip;
       
        if (RandomPlaybackPosition)
        {
            _source.timeSamples = Random.Range(0, _clip.samples - 1);
        }
        _source.Play();

    }

    // Update is called once per frame
    void Update()
    {
        currentTime += Time.deltaTime;
        LFOCurrentTime += Time.deltaTime;
        
        if (_curve.Evaluate(currentTime / fadetime) < 1)
        {
            _source.volume = volume * _curve.Evaluate(currentTime / fadetime);
            currentVolume = _source.volume;

        }
        else
        {
            if (LFOCurrentTime < LFOTime)
            {
                _source.volume =currentVolume* LFO.Evaluate(LFOCurrentTime / LFOTime);

            }
            else LFOCurrentTime = 0;   
        }
        
    }

    public void MusicDrone2()
    {
        AudioSource _source2 = gameObject.AddComponent<AudioSource>();
        _source2.outputAudioMixerGroup = group;
        _source2.volume = 0.8f;
        _source2.PlayOneShot(drone2);
    }
}
