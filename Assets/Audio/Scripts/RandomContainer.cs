using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using Random = UnityEngine.Random;


[RequireComponent(typeof(AudioSource))]
public class RandomContainer : MonoBehaviour
{
    
    
    public bool isLooping;
    public AudioClip[] RandomClips;
    public AudioSource AudioSource;
    public float RandomTime = 1f;
    [Range(-10, 0)] public float minRandomTime = 0f;
    [Range(0, 10)] public float maxRandomTime = 0f;
    
    
    [Tooltip("The MINIMUM Random Volume to playback")]
    [Range(0, 1)] public float minVolume = 1.0f;
    [Tooltip("The MAXIMUM Random Volume to playback")]
    [Range(0, 1)] public float maxVolume = 1.0f;
    [Tooltip("The MINIMUM Random Pitch to playback")]
    [Range(-3, 3)] public float minPitch = 1.0f;
    [Tooltip("The MAXIMUM Random Pitch to playback.")]
    [Range(-3, 3)] public float maxPitch = 1.0f;
    [Tooltip("Delay Sound playback in Seconds.")]
    public float PreDelay = 0;
    [Range(-10, 0)] public float minPreDelay = 0f;
    [Range(0, 10)] public float maxPreDelay = 0f;
    public bool drawSphereGizmo;
    public Color SPHERE_COLOR = new UnityEngine.Color(1.0f, 0.0f, 0.0f, 0.4f);
  
    
    
    
    
    
    
    
    int lastIndex = 0; // переменная для проверки какой был прошлый индекс сэмпла, чтобы не повторялся один и тот же сэмпл
    int newIndex = 0; // переменная для запоминания нового сформированного индекса сэмпла
    
    private void Awake()
    {
        AudioSource = GetComponent<AudioSource>();
    }

    private IEnumerator Loop()
    {
      
        while (true)
        {
           PlayRandomSound();
           var time = Random.Range(RandomTime - minRandomTime, RandomTime + maxRandomTime);
           yield return new WaitForSecondsRealtime(time);
        }
    }
  
    void Start()
    {
       
        
   
        if (isLooping)
        {
            StartCoroutine(Loop());
        }
        else
            PlayRandomSound();
        
        
            
            

    }

    private void OnDrawGizmosSelected()
    {
        if (drawSphereGizmo && AudioSource.spatialBlend == 1.0f)
        {
            Gizmos.color = SPHERE_COLOR;
            DrawSphere(transform.position, AudioSource.maxDistance);
        }
       
        
        
    }

  
    private void DrawSphere(UnityEngine.Vector3 in_position, float in_radius)
    {
        UnityEditor.Handles.color = SPHERE_COLOR;

        if ((UnityEditor.SceneView.lastActiveSceneView.camera.transform.position - in_position).sqrMagnitude > in_radius * in_radius)
        {
            UnityEditor.Handles.SphereHandleCap(0, in_position, UnityEngine.Quaternion.identity, in_radius * 2.0f, UnityEngine.EventType.Repaint);
        }
        else
        {
            DrawDiscs(UnityEngine.Vector3.up, UnityEngine.Vector3.down, 6, in_position, in_radius);
        }
    }
    
    private void DrawDiscs(UnityEngine.Vector3 in_startNormal, UnityEngine.Vector3 in_endNormal, uint in_nbDiscs,
        UnityEngine.Vector3 in_position, float in_radius)
    {
        var f = 1.0f / in_nbDiscs;
        for (var i = 0; i < in_nbDiscs; i++)
        {
            UnityEditor.Handles.DrawWireDisc(in_position, UnityEngine.Vector3.Slerp(in_startNormal, in_endNormal, f * i), in_radius);
        }
    }

    void PlayRandomSound()
    {
        
        
        if (RandomClips.Length != 1 )
                        Randomization(RandomClips.Length);
      
        AudioSource.volume = Random.Range(minVolume, maxVolume);
        AudioSource.pitch = Random.Range(minPitch, maxPitch);
        AudioSource.clip = RandomClips[newIndex];
        var playdelay = Random.Range(PreDelay - minPreDelay, PreDelay + maxPreDelay);
        AudioSource.PlayDelayed(playdelay);
        lastIndex = newIndex;
        
    }
    
    void Randomization(int cliplenght) // созданная нами функция для выбора случайного значения индекса сэмпла. в эту функицию мы передаем число сколько всего у нас сэмплов в массиве
    {
        
        newIndex = Random.Range(0, cliplenght);
        
        while (newIndex == lastIndex) // запускаем цикл, если новый индекс будет равен такому же числу как уже было = если сэмпл будет такой же, то
            newIndex = Random.Range(0, cliplenght); // снова запускаем рандом, пока не получим новое число для индекса
    }
    
    
    
    
}
