using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SplineMover : MonoBehaviour
{
    public SplineSoundEmitter spline;
    public Transform followObj;
    private Transform thisTransform;
    
    
    
    public AnimationCurve animationCurve;
    
    GameObject _camera;
    float volume;
    AudioSource _source;
    
    // Start is called before the first frame update
    void Start()
    {
        thisTransform = transform;
        _source = GetComponent<AudioSource>();
        volume = _source.volume;
         _camera = GameObject.FindWithTag("MainCamera");
    }

    // Update is called once per frame
    void Update()
    {
        thisTransform.position = spline.WhereOnSpline(followObj.position);
        
        
        
        Vector3 direction = _camera.transform.position - transform.position;
        float azimuth = (Vector3.Angle(_camera.transform.forward, direction)) / 180;
        
        _source.volume = volume * animationCurve.Evaluate(azimuth);
        
        
    }
}
