﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VOAudio : MonoBehaviour
{
    AudioSource _source;
    public AudioClip[] SpawnPhraseClip;
    public AudioClip[] JumpStartClip;
    public AudioClip[] LandClip;
    public AudioClip[] DamageClip;
    public AudioClip[] AttackStartClip;
    public AudioClip[] AttackEndClip;

    public float CooldownTime = 1f;
    float NextVoiceTime;


    private int last1Index = 0;
    private int last2Index = 0;
    private int last3Index = 0;
    private int newIndex = 0;

    bool isPlaying;


    void Start()
    {
        _source = GetComponent<AudioSource>();
    }

    void PlayVO(AudioClip clip)
    {
        if (!isPlaying)
        {
            _source.pitch = Random.Range(0.95f, 1.05f);
            _source.volume = Random.Range(0.7f, 1f);
            _source.PlayOneShot(clip);
            Index();
        }
        else isPlaying = false;


    }
   
    void LandVO()
    {
       
            Randomization(LandClip.Length);
            PlayVO(LandClip[newIndex]);
            
        

    }

    void JumpVO()
    {
        if (Time.time > NextVoiceTime)
        {
            Randomization(JumpStartClip.Length);
            PlayVO(JumpStartClip[newIndex]);
            NextVoiceTime = Time.time + CooldownTime;
        }

        }

    void Randomization(int cliplenght)
    {
        if (cliplenght != 0)
        {
            newIndex = Random.Range(0, cliplenght);

            while (newIndex == last1Index || newIndex == last2Index || newIndex == last3Index)
                newIndex = Random.Range(0, cliplenght);
        }
    }

    void DamageVO()
    {
        Randomization(DamageClip.Length);
        PlayVO(DamageClip[newIndex]);
        
    }

    void AttackStartVO()
    {
        if (Time.time > NextVoiceTime)
        {
            Randomization(JumpStartClip.Length);
            PlayVO(JumpStartClip[newIndex]);
            NextVoiceTime = Time.time + CooldownTime;
        }

    }

    void AttackEndVO()
    {
        Randomization(AttackEndClip.Length);
        PlayVO(AttackEndClip[newIndex]);
        
    }

    void Index()
    {
        last1Index = last2Index;
        last2Index = last3Index;
        last3Index = newIndex;
    }

}
