﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//
public class VegetationColliderAudio : MonoBehaviour
{

    public bool Leaves;
    public bool Grass;
    
    // Start is called before the first frame update
    void Start()
    {
     //   AudioSource Source = GetComponent<AudioSource>();  
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.tag == "Leaves")
        {
            Leaves = true;
        }
        else if (other.tag == "Grass")
        {
            Grass = true;
        }
        else
        {
            Leaves = false;
            Grass = false;
        }
    }

    
}
