﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponPhrase : MonoBehaviour
{
    public AudioClip[] weaponclip;
    private AudioSource _source;
    
    // Start is called before the first frame update
    void Start()
    {
        _source = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void PlayWeaponPhrase()
    {
        _source.clip = weaponclip[Random.Range(0, weaponclip.Length)];
        _source.PlayDelayed(Random.Range(1, 3f));
        _source.Play();
    }
}
