﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponPickupSound : MonoBehaviour
{
    public AudioSource loop;
    public AudioClip pickup;
    private AudioSource pickupsource;
    
    // Start is called before the first frame update
    void Start()
    {
        loop = GetComponent<AudioSource>();
        pickupsource = gameObject.AddComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void PickupSound()
    {
        loop.Stop();
        
        pickupsource.PlayOneShot(pickup);
        
    }
}
