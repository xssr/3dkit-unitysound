﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponSound : MonoBehaviour
{
    AudioSource _source;
    public AudioClip[] WeaponWhooshClip;
    

    public float CooldownTime = 1f;
    float NextVoiceTime;


    private int last1Index = 0;
    private int last2Index = 0;
    private int last3Index = 0;
    private int newIndex = 0;
    

    // Start is called before the first frame update
    void Start()
    {
        _source = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void WeaponWhooshSound()
    {
        if (Time.time > NextVoiceTime)
        {
            Randomization(WeaponWhooshClip.Length);
            PlayWeapon(WeaponWhooshClip[newIndex]);
            NextVoiceTime = Time.time + CooldownTime;
        }

    }

    void PlayWeapon(AudioClip clip)
    {
     
            _source.pitch = Random.Range(0.95f, 1.05f);
            _source.volume = Random.Range(0.7f, 1f);
            _source.PlayOneShot(clip);
            Index();
       


    }

    void Randomization(int cliplenght)
    {
        if (cliplenght != 0)
        {
            newIndex = Random.Range(0, cliplenght);

            while (newIndex == last1Index || newIndex == last2Index || newIndex == last3Index)
                newIndex = Random.Range(0, cliplenght);
        }
    }
    void Index()
    {
        last1Index = last2Index;
        last2Index = last3Index;
        last3Index = newIndex;
    }
}
